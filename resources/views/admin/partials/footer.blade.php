<div class="footer-left">
  Copyright &copy; {{ date('Y') }} <div class="bullet"></div> Design By <a href="https://sapat.com/">Sapat</a> | Project Implementation by <a href="https://twitter.com/yogesh369">Yogesh Pawar</a>
</div>
<div class="footer-right">
  2.3.0
</div>
